.. Cypress Documentation documentation master file, created by
   sphinx-quickstart on Sat Feb 22 20:00:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


###################################################
Cypress API documentation!
###################################################

RAWDATA API
===========
.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. doxygenstruct:: GATT
   :project: HelloWorld
   :members:

SET RAWDATA API 
===============
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
.. doxygenstruct:: SETGATT
   :project: HelloWorld
   :members:

UPDATE RAWDATA API
==================
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
.. doxygenstruct:: UPDATEGATT
   :project: HelloWorld
   :members:
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
